// Initialize dotenv
require('dotenv').config();

// NPM Libraries
const express = require('express');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const cors = require('cors');
// Mongoose
const mongoose = require('mongoose');

// Express configuration
const port = process.env.LISTENING_PORT;
// TODO Remove undefined in production, this is only for Postman
const whiteList = ['http://localhost:5002', 'http://localhost:3005', undefined];
const corsOptions = {
    credentials: true,
    origin: function (origin, callback) {
        if (whiteList.indexOf(origin) !== -1) {
            return callback(null, true)
        } else {
            callback(new Error("Not allowed by CORS"));
        }
    }
}

// Routers
const employeeRouter = require('./routers/EmployeeRouter');
const itemRouter = require('./routers/ItemRouter');
const customerRouter = require('./routers/CustomerRouter');
const vendorRouter = require('./routers/VendorRouter');
const inventoryRouter = require('./routers/InventoryRouter');
const internalOrderRouter = require('./routers/InternalOrderRouter');
const serverMessagesRouter = require('./routers/ServerMessages');

// Passport config
require('./authentication/auth');

main().catch(err => console.log(err));

async function main() {
    // Connection to db
    let db = await mongoose.connect(process.env.DB_URL);

    // Initialize express app
    const app = express();

    // Express App Uses
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    app.use(passport.initialize());
    app.use(cors(corsOptions));
    // app.use(cors({
    //     origin: true,
    //     credentials: true
    // }));
    // // For CORS
    // app.use(function(req, res, next) {
    //     res.header("Access-Control-Allow-Origin", "*");
    //     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    //     next();
    // })
    app.use(cookieParser());
    app.use('/api/server', serverMessagesRouter);
    app.use('/api/employees', employeeRouter);
    app.use('/api/items', itemRouter);
    app.use('/api/customers', customerRouter);
    app.use('/api/vendors', vendorRouter);
    app.use('/api/inventory', inventoryRouter);
    app.use('/api/internalOrders', internalOrderRouter);

    app.listen(port, () => {
        console.log("Inventory Manager Listening on Port: " + port);
    })
}