const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const Employee = require('../models/Employee');
const Customer = require('../models/Customer');
const encrypt = require('../utils/encrypt');

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.JWT_SECRET;

passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    let ut = encrypt.decryptString(jwt_payload.userType);

    // Check the user type for customer, employee, and something else.
    if (ut === 'customer') {
        Customer.findOne({ id: jwt_payload.sub })
        .then(customer => {
            if (customer) {
                return done(null, {customer});
            } else {
                return done(null, false, { msg: "Incorrect password." });
            }
        })
        .catch(err => {
            console.log(err);
            return done(err, false, { msg: "Not valid credentials."});
        })
    }
    else if (ut === 'employee') {
        Employee.findOne({ id: jwt_payload.sub })
        .then(employee => {
            if (employee) {
                return done(null, {employee});
            } else {
                return done(null, false, { msg: "Incorrect password." });
            }
        })
        .catch(err => {
            console.log(err);
            return done(err, false, { msg: "Not valid credentials."});
        })
    }
    else {
        return done(null, false, { msg: "Invalid request." });
    }
}));