const express = require('express');
const router = express.Router();

router.get("/unauthorized", function(req, res) {
    res.status(401).send({
        msg: "You are unauthorized to perform this action"
    });

});

module.exports = router;