const express = require('express');
const Customer = require('../models/Customer');
const router = express.Router();
const bcrypt = require('bcrypt');
const saltRounds = 10;
const passport = require('passport');
const encrypt = require('../utils/encrypt');

router.get("/", async function(req, res, next) {
    try {
        let customers = await Customer.find()
        res.send(customers);
    } catch(e) {
        res.send(e);
        next(e);
    }
});

router.get("/authTest", passport.authenticate('jwt', { session: false}), async function(req, res, next) {
    try {
        let customer = await Customer.find()
        res.send(customer);
    } catch(e) {
        console.log(e);
        res.send(e);
    }
});

router.post("/register", async function(req, res, next) {
    try {
        // Encrypt the type of user in order to include it in the token
        req.body.userType = encrypt.encryptString("customer");
        
        let customer = new Customer(req.body);
        // Hash password
        customer.password = await bcrypt.hash(customer.password, saltRounds);

        let result = await customer.save();
        // Save the Customer and generate a token

        const token = result.generateJWT();
        let data = {
            msg: "success",
            result,
            token
        };
        res.send(data);

    } catch(e) {
        res.status(500).send({
            msg: "There was an error.",
            error: e
        });
        next(e);
    }
});

router.post("/login", async function(req, res, next) {
    try {
        let customer = await Customer.findOne({ email: req.body.email });

        if (customer == null) {
            res.status(404).send({ msg: "Invalid username or password." });
        } else {
            // Check to see if the Customer entered the correct password
            const match = await bcrypt.compare(req.body.password, customer.password);

            if (match) {
                const token = customer.generateJWT();
                let data = {
                    msg: "success",
                    token
                };
                res.status(200).send(data);
            } else {
                res.status(404).send({
                    msg: "No Customer found with that email and password combination."
                });
            }
        }
    } catch (e) {
        res.send(e);
        next(e);
    }
})

router.delete("/delete", async function(req, res, next) {
    try {
        let results = await Customer.findByIdAndDelete(req.body.id);
        if (results) {
            res.status(200).send({
                msg: "Customer successfully deleted.",
                data: results
            });
        } else {
            res.status(404).send({
                msg: "No Customer found with that id."
            })
        }

    } catch(e) {
        res.status(500).send({error: e});
        next(e);
    }
});

// router.get("/:id", async function(req, res, next) {
//     try {
//         results = await Customer.findById(req.params.id);
//         res.send(results);
//     } catch(e) {
//         res.send(e);
//         next(e);
//     }
// });

module.exports = router;