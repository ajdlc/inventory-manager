const express = require('express');
const Item = require('../models/Item');
const passport = require('passport');
const router = express.Router();

router.get("/", async function(req, res, next) {
    try {
        let items = await Item.find().populate('vendor');
        res.send(items);
    } catch(e) {
        res.send(e);
        next(e);
    }
});

router.post("/", passport.authenticate('jwt', { session: false}), async function(req, res, next) {
    if (req.user.hasOwnProperty('employee')) {
        let item = new Item(req.body);
        try {
    
            let result = await item.save();
            res.send(result);
    
        } catch(e) {
            res.status(500).send({
                msg: "There was an error.",
                error: e
            });
            next(e);
        }
    } else {
        res.status(401).send({ msg: "You are not authorized to perform this action."});
    }
});

router.post("/search", async function(req, res) {
    try {
        let item = await Item.find(req.body);

        if (item) {
            res.status(200).send(item);
        } else {
            res.status(404).send({
                msg: "No item found."
            });
        }
    } catch (e) {
        res.send(e);
    }
})

router.get("/:id", async function(req, res, next) {
    try {
        let item = await Item.findById(req.params.id).populate('vendor');
        res.send(item);
    } catch(e) {
        res.send(e);
    }
});

router.patch("/:id", passport.authenticate('jwt', { session: false}), async function(req, res, next) {
    if (req.user.hasOwnProperty('employee')) {
        try {
            let item = await Item.findByIdAndUpdate(req.params.id, req.body);
    
            if (item) {
                res.status(200).send(item);
            } else {
                res.status(404).send({
                    msg: "No item found."
                });
            }
        } catch (e) {
            if (e.name === 'CastError') {
                res.status(400).send({ msg: "Your request is invalid. Please check your ID string."})
            } else {
                res.send(e);
            }
        }
    } else {
        res.status(401).send({ msg: "You are not authorized to perform this action."});
    }
})

router.delete("/:id", passport.authenticate('jwt', { session: false}), async function(req, res) {
    if (req.user.hasOwnProperty('employee')) {
        try {
            let results = await Item.findByIdAndDelete(req.params.id);
            if (results) {
                res.status(200).send({
                    msg: "Item successfully deleted.",
                    data: results
                });
            } else {
                res.status(404).send({
                    msg: "No item found with that id."
                })
            }
    
        } catch(e) {
            res.status(500).send({error: e});
        }
    } else {
        res.status(401).send({ msg: "You are not authorized to perform this action."});
    }
});

module.exports = router;