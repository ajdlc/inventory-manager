const express = require('express');
const InternalOrders = require('../models/InternalOrder');
const passport = require('passport');
const router = express.Router();

router.get("/", passport.authenticate('jwt', { session: false}), async function(req, res, next) {
    if (req.user.hasOwnProperty('employee')) {
        try {
            let orders = await InternalOrders.find().populate('items');
            if (orders.length > 0) {
                res.status(200).send(orders);
            } else {
                res.status(404).send({ msg: "No orders found." });
            }
    
        } catch (e) {
            res.send(e);
        }
    } else {
        res.status(401).send({ msg: "You are not authorized to perform this action."});
    }
});

router.post("/", passport.authenticate('jwt', { session: false}), async function(req, res) {
    if (req.user.hasOwnProperty('employee')) {
        try {
            let order = new InternalOrders(req.body);
    
            let result = await order.save();
            res.send(result);
    
        } catch(e) {
            res.status(500).send({
                msg: "There was an error.",
                error: e
            });
        }
    } else {
        res.status(401).send({ msg: "You are not authorized to perform this action."});
    }
});

router.get("/:id", passport.authenticate('jwt', { session: false}), async function(req, res, next) {
    if (req.user.hasOwnProperty('employee')) {
        try {
            let order = await InternalOrders.findById(req.params.id).populate('items');
            if (order) {
                res.status(200).send(order);
            } else {
                res.status(404).send({ msg: "No order found." });
            }
    
        } catch (e) {
            res.send(e);
        }
    } else {
        res.status(401).send({ msg: "You are not authorized to perform this action."});
    }
});

router.patch("/:id", passport.authenticate('jwt', { session: false}), async function(req, res, next) {
    if (req.user.hasOwnProperty('employee')) {
        try {
            let order = await InternalOrders.findByIdAndUpdate(req.params.id, req.body);
            if (order) {
                res.status(200).send(order);
            } else {
                res.status(404).send({ msg: "No order found." });
            }
    
        } catch (e) {
            res.send(e);
        }
    } else {
        res.status(401).send({ msg: "You are not authorized to perform this action."});
    }
});

router.delete("/:id", passport.authenticate('jwt', { session: false}), async function(req, res, next) {
    if (req.user.hasOwnProperty('employee')) {
        try {
            let order = await InternalOrders.findByIdAndDelete(req.params.id);
            if (order) {
                res.status(200).send({ msg: "Deleted successfully.", order});
            } else {
                res.status(404).send({ msg: "No order found." });
            }
    
        } catch (e) {
            res.send(e);
        }
    } else {
        res.status(401).send({ msg: "You are not authorized to perform this action."});
    }
});

// router.delete("/delete", async function(req, res, next) {
//     try {
//         let results = await Item.findByIdAndDelete(req.body.id);
//         if (results) {
//             res.status(200).send({
//                 msg: "Item successfully deleted.",
//                 data: results
//             });
//         } else {
//             res.status(404).send({
//                 msg: "No item found with that id."
//             })
//         }

//     } catch(e) {
//         res.status(500).send({error: e});
//         next(e);
//     }
// });

module.exports = router;