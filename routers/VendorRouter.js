const express = require('express');
const Vendors = require('../models/Vendor');
const router = express.Router();
const bcrypt = require('bcrypt');
const saltRounds = 10;
const passport = require('passport');

router.get("/", async function(req, res) {
    try {
        let vendors = await Vendors.find()
        res.send(vendors);
    } catch(e) {
        res.send(e);
    }
});

router.post("/search", async function(req, res) {
    try {
        let vendors = await Vendors.find(req.body)
        res.send(vendors);
    } catch(e) {
        res.send(e);
    }
});

router.post("/create", passport.authenticate('jwt', { session: false}), async function(req, res) {
    let vendor = new Vendors(req.body);

    try {
        let result = await vendor.save();
        res.send(result);

    } catch(e) {
        res.status(500).send({
            msg: "There was an error.",
            error: e
        });
    }
});

router.delete("/delete", passport.authenticate('jwt', { session: false}), async function(req, res) {
    try {
        let results = await Vendors.findByIdAndDelete(req.body.id);
        if (results) {
            res.status(200).send({
                msg: "Customer successfully deleted.",
                data: results
            });
        } else {
            res.status(404).send({
                msg: "No Customer found with that id."
            })
        }

    } catch(e) {
        res.status(500).send({error: e});
    }
});

module.exports = router;