const express = require('express');
const Inventories = require('../models/Inventory');
const Item = require('../models/Item');
const passport = require('passport');
const router = express.Router();

router.get("/", passport.authenticate('jwt', { session: false}), async function(req, res, next) {
    let id = req.user.employee.id;

    try {
        let inventory = await Inventories.findOne({ employeeId: id}).populate('items.item');

        if (inventory) {
            res.status(200).send(inventory);
        } else {
            res.status(404).send({ msg: "No inventory found for this user." });
        }

    } catch (e) {
        res.send(e);
    }
});

// router.post("/company", passport.authenticate('jwt', { session: false}), async function(req, res) {
//     let item = req.body;

//     // Verify that there is enough quantity of the item
//     // Add the item to the users inventory
//     // Remove the quantity from the item

//     try {

//         let result = await item.save();
//         res.send(result);

//     } catch(e) {
//         res.status(500).send({
//             msg: "There was an error.",
//             error: e
//         });
//         next(e);
//     }
// });

router.post("/employee/:id", passport.authenticate('jwt', { session: false}), async function(req, res) {
    try {
        if (req.user.employee.id === req.params.id) {
            // Check to see if an inventory already exists
            let inventory = await Inventories.findOne({ employeeId: req.params.id});

            if (inventory) {
                res.status(400).send({ msg: "An inventory exists for this user.", inventory });
            } else {
                let newInv = await new Inventories({ employeeId: req.params.id });

                // Save the new inventory
                let savedInv = await newInv.save();
    
                res.status(200).send(savedInv);
            }
        } else {
            res.status(401).send({ msg: "You are not authorized to update this users inventory." });
        }
    } catch(e) {
        res.status(500).send({
            msg: "There was an error.",
            error: e
        });
    }
});

router.delete("/employee/:id", passport.authenticate('jwt', { session: false}), async function(req, res) {
    try {
        if (req.user.employee.id === req.params.id) {
            // Check to see if an inventory already exists
            let inventory = await Inventories.findOne({ employeeId: req.params.id});

            if (inventory) {
                // Update the currentQuantity for each item in the inventory
                for (item of inventory.items) {
                    // Get the Item
                    let dbItem = await Item.findById(item.item);

                    // Update its currentQuantity
                    dbItem.currentQuantity += item.quantity;

                    // Save the changes
                    await dbItem.save();
                }

                await inventory.remove();
                res.status(200).send(inventory);

            } else {
                res.status(400).send({ msg: "An inventory does not exists for this user." });
            }
        } else {
            res.status(401).send({ msg: "You are not authorized to update this users inventory." });
        }
    } catch(e) {
        console.log(e);
        res.status(500).send({
            msg: "There was an error.",
            error: e
        });
    }
});

router.post("/employee/:id/item/:itemId", passport.authenticate('jwt', { session: false}), async function(req, res) {
    let qty = parseInt(req.body.quantity);

    try {
        let item = await Item.findById(req.params.itemId);

        // Verify that there is enough quantity of the item
        if ((item.currentQuantity - qty) > 0) {
            let inventory = await Inventories.findOne({ employeeId: req.params.id});

            // Check to see if an inventory is found, if none found, make one.
            if (inventory) {
                // Update the inventory
                inventory.items.push(
                    {
                        item: item.id, 
                        quantity: qty
                    }
                );
                let updatedInv = await inventory.save();

                // Update the currentQuantity of the item
                item.currentQuantity = item.currentQuantity - qty;
                let updatedItem = await item.save();

                res.status(200).send(updatedInv);
            } else {
                // Confirm the user updating the inventory is the owner of the inventory
                // TODO For the future allow admins to update inventory or supervisors.
                if (req.user.employee.id === req.params.id) {
                    let newInv = await new Inventories({
                        employeeId: req.params.id,
                        items: [
                            {
                                item: item.id,
                                quantity: qty
                            }
                        ]
                    });

                    // Save the new inventory
                    let savedInv = await newInv.save();

                    // Update the currentQuantity of the item
                    item.currentQuantity = item.currentQuantity - qty;
                    await item.save();

                    res.status(200).send(savedInv);
                } else {
                    res.status(401).send({ msg: "You are not authorized to update this users inventory." });
                }
            }
        } else {
            res.status(400).send({ 
                msg: `There are not enough items available. There are only: ${item.currentQuantity} left.`,
                currentQuantity: item.currentQuantity,
                requestedQuantity: qty
            });
        }
    } catch(e) {
        res.status(500).send({
            msg: "There was an error.",
            error: e
        });
    }
});

router.patch("/employee/:id/item/:itemId", passport.authenticate('jwt', { session: false}), async function(req, res) {
    let qty = parseInt(req.body.quantity);

    try {
        let item = await Item.findById(req.params.itemId);

        // Verify that there is enough quantity of the item
        if ((item.currentQuantity - qty) > 0) {
            let inventory = await Inventories.findOne({ employeeId: req.params.id});

            // Check to see if an inventory is found, if none found, make one.
            if (inventory) {
                if (inventory.items.length > 0) {
                    // Update the inventory
                    for (let i = 0; i < inventory.items.length; i++) {
                        if (inventory.items[i].item == item.id) {
                            inventory.items[i].quantity += qty;
                        }
                    }

                    let updatedInv = await inventory.save();

                    // Update the currentQuantity of the item
                    item.currentQuantity = item.currentQuantity - qty;
                    let updatedItem = await item.save();

                    res.status(200).send(updatedInv);
                } else {
                    res.status(400).send({ msg: "There are no items in the inventory to update." })
                }
            } else {
                // Confirm the user updating the inventory is the owner of the inventory
                if (req.user.employee.id === req.params.id) {
                    res.status(400).send({ msg: "You need to create an inventory first by adding an item to it." });
                } else {
                    res.status(401).send({ msg: "You are not authorized to update this users inventory." });
                }
            }
        } else {
            res.status(400).send({ 
                msg: `There are not enough items available. There are only: ${item.currentQuantity} left.`,
                currentQuantity: item.currentQuantity,
                requestedQuantity: qty
            });
        }
    } catch(e) {
        res.status(500).send({
            msg: "There was an error.",
            error: e
        });
    }
});

router.delete("/employee/:id/item/:itemId", passport.authenticate('jwt', { session: false}), async function(req, res) {
    try {
        let item = await Item.findById(req.params.itemId);
        let qty = 0;
        let index = 0;

        // Verify that there is enough quantity of the item
        if (item) {
            let inventory = await Inventories.findOne({ employeeId: req.params.id});

            // Check to see if an inventory is found, if none found, make one.
            if (inventory) {
                if (inventory.items.length > 0) {
                    // Update the inventory
                    for (let i = 0; i < inventory.items.length; i++) {
                        if (inventory.items[i].item == item.id) {
                            qty = inventory.items[i].quantity;
                            index = i;
                        }
                    }

                    // Remove the item
                    inventory.items.splice(index, 1);

                    let updatedInv = await inventory.save();

                    // Update the currentQuantity of the item
                    item.currentQuantity = item.currentQuantity + qty;
                    await item.save();

                    res.status(200).send(updatedInv);
                } else {
                    res.status(400).send({ msg: "There are no items in the inventory to delete." });
                }
            } else {
                // Confirm the user updating the inventory is the owner of the inventory
                if (req.user.employee.id === req.params.id) {
                    res.status(400).send({ msg: "You need to create an inventory first by adding an item to it." });
                } else {
                    res.status(401).send({ msg: "You are not authorized to update this users inventory." });
                }
            }
        } else {
            res.status(400).send({ msg: `No items found in the inventory with the id of: ${item.id}` });
        }
    } catch(e) {
        console.log(e);
        res.status(500).send({
            msg: "There was an error.",
            error: e
        });
    }
});

module.exports = router;