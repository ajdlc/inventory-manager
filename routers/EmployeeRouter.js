const express = require('express');
const Employee = require('../models/Employee');
const router = express.Router();
const bcrypt = require('bcrypt');
const saltRounds = 10;
const passport = require('passport');
const encrypt = require('../utils/encrypt');
const jwtPackage = require('jsonwebtoken');

const sendRefreshToken = require('../utils/route-utils/sendRefreshToken');

router.get("/", async function(req, res, next) {
    try {
        let users = await Employee.find()
        res.send(users);
    } catch(e) {
        res.send(e);
        next(e);
    }
});

router.get("/authTest", passport.authenticate('jwt', { session: false, failureRedirect: '/api/server/unauthorized' }), async function(req, res, next) {
    try {
        let users = await Employee.find()
        res.send(users);
    } catch(e) {
        console.log(e);
        res.send(e);
    }
});

router.post("/forgotPassword", async function(req, res) {
    try {
        let user = await Employee.findOne(req.body);
        
        if (user) {
            let link = "";
            // Create the JWT and the link
            let jwt = user.generatePasswordResetJWT();

            link = `employees/resetPassword?token=${jwt}`;

            // TODO - Remove this once in production.
            res.status(200).send({ msg: "Reset link sent to email.", link})
            // Email it to the user - TODO - Add this later
        } else {
            res.status(404).send({ msg: "No user exists with that email address." })
        }
    } catch (err) {
        console.log(err);
        res.status(500).send(err);
    }
});

router.post("/resetPassword", async function(req, res) {
    let token = "";
    try {
        token = encrypt.decryptString(req.query.token);
        let unverifiedPayload = jwtPackage.decode(token);

        let employee = await Employee.findById(unverifiedPayload.sub);

        if (employee) {
            let secret = employee.getPasswordResetSecret();
            try {
                let decoded = jwtPackage.verify(token, secret);
                // Update their password
                employee.password = await bcrypt.hash(req.body.password, saltRounds);
                // Save the employee
                let updatedEmployee = await employee.save();

                res.status(200).send({ msg: "Password updated.", updatedEmployee });
                
            } catch (err) {
                res.status(400).send({ msg: "Invalid token." })
            }
        } else {
            res.status(404).send({ msg: "No employee found." });
        }
    } catch (err) {
        res.status(400).send({ msg: "Invalid token." })
    }
});

router.post("/register", async function(req, res, next) {
    // Encrypt the type of user in order to include it in the token
    req.body.userType = encrypt.encryptString("employee");

    try {
        let user = new Employee(req.body);
        // Hash password
        user.password = await bcrypt.hash(user.password, saltRounds);

        let result = await user.save();
        // Save the user and generate a token

        const token = result.generateJWT();
        let data = {
            msg: "success",
            result,
            token
        };
        res.send(data);

    } catch(e) {
        res.status(500).send({
            msg: "There was an error.",
            error: e
        });
        next(e);
    }
});

router.post("/login", async function(req, res, next) {
    try {
        let user = await Employee.findOne({ email: req.body.email });
        
        if (user == null) {
            res.status(404).send({ msg: "Invalid username or password." });
        } else {
            // Check to see if the user entered the correct password
            const match = await bcrypt.compare(req.body.password, user.password);

            if (match) {
                // Generate a refresh token and store in Cookie
                sendRefreshToken(res, user.generateRefreshJWT());
                // res.cookie('rfToken', 
                // user.generateRefreshJWT(), 
                // {
                //     httpOnly: true
                // });

                const token = user.generateJWT();
                let data = {
                    msg: "success",
                    id: user.id,
                    token
                };
                res.status(200).send(data);
            } else {
                res.status(404).send({
                    msg: "No user found with that email and password combination."
                });
            }
        }
    } catch (e) {
        res.send(e);
    }
})

router.post("/refresh", async function(req, res) {
    const token = req.cookies.rfToken;
    let payload = null;

    if (token) {
        // Confirm a valid token
        try {
            payload = jwtPackage.verify(token, process.env.JWT_REFRESH_SECRET);
            // Find the user
            const user = await Employee.findOne({ id: payload.sub });

            if (user) {
                // Confirm the refresh token has not been revoked
                if (user.tokenVersion == payload.tokenVersion) {
                    // Generate a refresh token and store in Cookie
                    sendRefreshToken(res, user.generateRefreshJWT());

                    res.send({ token: user.generateJWT() });
                } else {
                    res.status(400).send({ msg: "Token is not valid."});
                }
            } else {
                res.status(404).send({ msg: "No user found. Please try again" });
            }
        } catch (error) {
            res.status(400).send({ msg: "Token is not valid.", error: error.message });
        }

    } else {
        res.status(400).send({ msg: "No valid cookie found in request." });
    }
})

router.delete("/delete", async function(req, res, next) {
    try {
        let results = await Employee.findByIdAndDelete(req.body.id);
        if (results) {
            res.status(200).send({
                msg: "User successfully deleted.",
                data: results
            });
        } else {
            res.status(404).send({
                msg: "No user found with that id."
            })
        }

    } catch(e) {
        res.status(500).send({error: e});
        next(e);
    }
});

// router.get("/:id", async function(req, res, next) {
//     try {
//         results = await User.findById(req.params.id);
//         res.send(results);
//     } catch(e) {
//         res.send(e);
//         next(e);
//     }
// });

module.exports = router;