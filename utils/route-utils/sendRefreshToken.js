const sendRefreshToken = (res, token) => {
    res.cookie('rfToken', 
    token, 
    {
        httpOnly: true
    });
};

module.exports = sendRefreshToken;