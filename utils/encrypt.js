/**
 * *********************************************************************************************************
 * This is adapted from the following URL:
 *  https://www.section.io/engineering-education/data-encryption-and-decryption-in-node-js-using-crypto/
 * 
 * *********************************************************************************************************
 */

const crypto = require('crypto');
// Have to get the buffer of the secrety key
const securityKey = Buffer.from(process.env.ENCRYPT_SECURITY_KEY, "hex");
const algorithm = "aes-256-cbc";
const initVector = Buffer.from(process.env.ENCRYPT_INIT_VECTOR, "hex");

const encrypt = {
    encryptString(message) {
        const cipher = crypto.createCipheriv(algorithm, securityKey, initVector);

        let encryptedData = cipher.update(message, "utf-8", "hex");
        encryptedData += cipher.final("hex");

        return encryptedData;
    },
    decryptString(encryptedData) {
        const decipher = crypto.createDecipheriv(algorithm, securityKey, initVector);

        let decryptedData = decipher.update(encryptedData, "hex", "utf-8");
        decryptedData += decipher.final("utf-8");

        return decryptedData;
    }
};

module.exports = encrypt;