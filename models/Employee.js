const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const encrypt = require('../utils/encrypt');
// const crypto = require('crypto');

const employeeSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        trim: true
    },
    lastName: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        index: true,
        unique: true,
        required: true,
        trim: [true, "An email address is required."]
    },
    password: {
        type: String,
        required: [true, "A password is required."]
    },
    bio: {
        type: String
    },
    dateHired: {
        type: Date,
        default: Date.now
    },
    status: {
        type: String,
        enum: ["Current", "Quit", "Retired", "Fired", "Contractor"],
        required: [true, "An employee status is required."]
    },
    isAdmin: {
        type: Boolean,
        required: [true, "Administrator status required."]
    },
    userType: {
        type: String,
        required: true
    },
    tokenVersion: {
        type: Number,
        default: 0
    }
}, {timestamps: true});

employeeSchema.virtual('fullName').get(function() {
    return this.firstName + " " + this.lastName;
});

employeeSchema.methods.generateJWT = function() {
    const expiresIn = '15m';

    return jwt.sign({
        sub: this.id,
        userType: this.userType
    }, process.env.JWT_SECRET, { expiresIn })
};

employeeSchema.methods.generateRefreshJWT = function() {
    const expiresIn = '7d';

    return jwt.sign({
        sub: this.id,
        userType: this.userType,
        tokenVersion: this.tokenVersion
    }, process.env.JWT_REFRESH_SECRET, { expiresIn })
};

employeeSchema.methods.incrementTokenVersion = function() {
    this.tokenVersion = this.tokenversion + 1;
};

employeeSchema.methods.generatePasswordResetJWT = function() {
    const secret = this.password + "-" + this.createdAt;
    const expiresIn = '4h';

    let token = jwt.sign({
        sub: this.id
    }, secret, { expiresIn });

    return encrypt.encryptString(token);
};

employeeSchema.methods.getPasswordResetSecret = function() {
    return this.password + "-" + this.createdAt;
};

module.exports = mongoose.model('Employee', employeeSchema);