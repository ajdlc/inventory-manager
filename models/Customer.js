const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
// const crypto = require('crypto');

const customerSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        trim: true
    },
    lastName: {
        type: String,
        required: true,
        trim: true
    },
    phone: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        index: true,
        unique: true,
        required: true,
        trim: [true, "An email address is required."]
    },
    password: {
        type: String,
        required: [true, "A password is required."]
    },
    streetAddress: {
        type: String,
        trim: true
    },
    city: {
        type: String,
        trim: true
    },
    state: {
        type: String,
        trim: true
    },
    zipCode: {
        type: String,
        trim: true
    },
    userType: {
        type: String,
        required: true
    }
}, {timestamps: true});

customerSchema.virtual('fullName').get(function() {
    return this.firstName + " " + this.lastName;
});

customerSchema.virtual('fullAddress').get(function() {
    return this.streetAddress + "\n" + this.city + ", " + this.state + " " + this.zipCode;
});

customerSchema.methods.generateJWT = function() {
    const expiresIn = '1h';

    return jwt.sign({
        sub: this.id,
        userType: this.userType
    }, process.env.JWT_SECRET, { expiresIn })
}

module.exports = mongoose.model('Customer', customerSchema);