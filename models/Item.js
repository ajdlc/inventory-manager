const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "A name is required."],
        trim: true
    },
    model: {
        type: String,
        trim: true
    },
    serialNumber: {
        type: String,
        trim: true
    },
    manufacturer: {
        type: String,
        trim: true
    },
    location: {
        type: String,
        trim: true,
        required: [true, "A location for the item is required."]
    },
    originalQuantity: {
        type: Number,
        default: 1,
        min: 1,
        required: [true, "A starting quantity is needed."]
    },
    currentQuantity: {
        type: Number
    },
    vendor: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Vendors'
    }
}, {timestamps: true});

// Set the default for currentQuantity to the originalQuantity
itemSchema.pre('save', function(next) {
    if (!this.currentQuantity) {
        this.currentQuantity = this.get('originalQuantity');
    }
    next();
})


module.exports = mongoose.model('Item', itemSchema);