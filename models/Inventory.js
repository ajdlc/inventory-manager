const mongoose = require('mongoose');

const inventorySchema = new mongoose.Schema({
    employeeId: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Vendors'
    },
    items: [
        {
            item: {
                type: mongoose.Schema.Types.ObjectId, ref: 'Item'
            },
            quantity: {
                type: Number,
                required: [true, "A quantity is required."]
            }
        }
    ]
}, {timestamps: true});

module.exports = mongoose.model('Inventories', inventorySchema);