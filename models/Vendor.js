const mongoose = require('mongoose');

const vendorSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "A name is required."],
        trim: true
    },
    online: {
        type: Boolean,
        required: true
    },
    phoneNumber: {
        type: String,
        trim: true
    },
    streetAddress: {
        type: String,
        trim: true
    },
    city: {
        type: String,
        trim: true
    },
    state: {
        type: String,
        trim: true
    },
    zipCode: {
        type: String,
        trim: true
    },
    code: {
        type: String,
        required: [true, "A unique vendor code is required."],
        trim: true
    }
}, {timestamps: true});

vendorSchema.virtual('fullAddress').get(function() {
    return this.streetAddress + "\n" + this.city + ", " + this.state + " " + this.zipCode;
});

module.exports = mongoose.model('Vendors', vendorSchema);