const mongoose = require('mongoose');

const internalOrdersSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: [true, "A name is required."]
    },
    orderNumber: {
        type: String,
        trim: true
    },
    vendor: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Vendors'
    },
    cost: {
        total: {
            type: Number,
            required: [true, "A total cost is required."]
        },
        subTotal: {
            type: Number
        },
        taxes: {
            type: Number
        }
    },
    items: [
        {
            type: mongoose.Schema.Types.ObjectId, ref: 'Item'
        }
    ],
    notes: {
        type: String
    }
}, {timestamps: true});

module.exports = mongoose.model('InternalOrders', internalOrdersSchema);